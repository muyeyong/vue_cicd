FROM node:10

# 创建工作目录，对应的是应用代码存放在容器内的路径
WORKDIR /usr/src/app

# 把 package.json，package-lock.json(npm@5+) 或 yarn.lock 复制到工作目录(相对路径)
COPY package.json *.lock /usr/src/app

RUN npm install pm2 -g --registry=https://registry.npm.taobao.org
# 只安装dependencies依赖
# node镜像自带yarn
RUN npm install --registry=https://registry.npm.taobao.org

# 把其他源文件复制到工作目录
COPY . /usr/src/app

# 替换成应用实际的端口号
EXPOSE 80

# 这里根据实际起动命令做修改
CMD [ "pm2-runtime", "index.js", "--watch" ]